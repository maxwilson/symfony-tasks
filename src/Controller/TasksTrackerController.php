<?php

namespace App\Controller;


use App\Domain\Model\Projeto;
use App\Domain\Model\Status;
use App\Domain\Model\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/task")
 */
class TasksTrackerController extends AbstractController
{
    
    /**
     * @Route("/teste/{nome}")
     */
    public function teste(string $nome): Response
    {
        
        $projeto = new Projeto();
        $projeto->setNome($nome);
        $projeto->setDescricao('Descrição do projeto');
        $projeto->setDtCadastro(new \DateTime());
        $projeto->setGerente('Gerente');

        $em = $this->getDoctrine()->getManager();

        $em->persist($projeto);
        $em->flush();


        $projetos = $em->getRepository(Projeto::class)->findAll();

        dump($projetos);exit;

        return new Response('bem vindo '. $nome);
    }

}
